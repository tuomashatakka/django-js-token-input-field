from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.forms import Form
from django.forms.fields import CharField
from .fields import BaseTokenInputField


class ExampleForm(Form):
    basic = BaseTokenInputField({
        '1': 'Helsinki',
        '2': 'Stockholm',
        '3': 'Malmö',
        '4': 'Berlin',
        '5': 'Atacama',
    })
    namee = CharField()


class DefaultFormView(FormView):
    template_name = 'default.html'
    form_class = ExampleForm

    def get_context_data(self, *args, **kwargs):
        ctx = super(DefaultFormView, self).get_context_data(*args, **kwargs)
        return ctx

    def form_valid(self, form):
        return super(DefaultFormView, self).form_valid(form)
