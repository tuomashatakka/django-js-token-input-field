from django.forms.widgets import Input, Widget
from django.forms.fields import CharField, Field

from django.forms.utils import flatatt
from django.utils.datastructures import MultiValueDict
from django.utils.html import conditional_escape, format_html, html_safe
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.core.signals import request_started
from django.template.response import TemplateResponse


def fnc(request):
    print(request)
    return "LORSSII"


class PostRenderMiddleware(object):
    def process_template_response(self, request, response):
        response.content = response.rendered_content + '''
        <div></div>
        '''
        response.add_post_render_callback(fnc)
        # print(response.rendered_content)
        return response


class BaseTokenInputWidget(Widget):
    input_type = 'text'
    html_classes = ['token-input', ]
    render_template = '''
    <div class="token-input-wrap">
        <ul class='token-input-selected-list'>
            <li class='token-input-selected-current'>
                <input class='token-input'{attrs} />
            </li>
        </ul>
        <div class='token-input-deets'>
            <select name='{name}' multiple>{initial}</select>
            <div class='token-input-dropdown'></div>
        </div>
    </div>
    '''

    def __init__(self, attrs=None):
        if attrs is not None:
            self.input_type = attrs.pop('type', self.input_type)
        else:
            attrs = {}
        attrs['class'] = ' '.join(self.html_classes)

        # Bind to the request_started signal
        request_started.connect(self.render_js)

        super(BaseTokenInputWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            final_attrs['value'] = force_text(self._format_value(value))
        return format_html(
            self.render_template,
            attrs=flatatt(final_attrs),
            name=name,
            initial=mark_safe(self.render_options()),
        )

    def render_options(self, **kwargs):
        html = ""
        for key, val in self.initial_value.items():
            html += format_html(
                "<option value='{key}'>{val}</option>",
                key=key,
                val=val,
            )
        return html

    def render_js(self, sender, environ, **kwargs):
        # print("sender")
        # print(sender)
        # print(environ)
        print(kwargs)


class BaseTokenInputField(CharField):
    widget = BaseTokenInputWidget
    initial_value = ""
    placeholder = "placeholding yess!"

    def __init__(self, initial_value={}, **args):
        self.widget.initial_value = initial_value
        self.initial_value = initial_value
        super(BaseTokenInputField, self).__init__(**args)
        self.placeholder = args.get('placeholder', "")
