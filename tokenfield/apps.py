from django.apps import AppConfig


class TokenfieldApplication(AppConfig):
    name = 'tokenfield'
